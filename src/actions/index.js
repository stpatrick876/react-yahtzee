import {
  EDIT_PLAYER,
  ROLL_DIE,
  SET_SCORE,
  SWITCH_TURN,
  START_GAME
} from '../constants/action-types';

export function editPlayer(payload) {
  return { type: EDIT_PLAYER, payload };
}

export function diceRoll(values) {
  return { type: ROLL_DIE, values };
}

export function setScore(payload) {
  return { type: SET_SCORE, payload };
}

export function switchTurn() {
  return { type: SWITCH_TURN };
}

export function startGame() {
  return { type: START_GAME };
}
