import {
  EDIT_PLAYER,
  ROLL_DIE,
  SET_SCORE,
  SWITCH_TURN,
  START_GAME
} from '../constants/action-types';
import { initScores, sumSection } from '../utils';
import { GAME_STATUS, TURN_STATUS } from '../constants/game';

export const PLAYER_ONE = 'one';
export const PLAYER_TWO = 'two';

const initialState = {
  players: [
    {
      id: PLAYER_ONE,
      name: 'Player 1',
      scores: initScores(),
      total: { upper: 0, lower: 0 }
    },
    {
      id: PLAYER_TWO,
      name: 'Player 2',
      scores: initScores(),
      total: { upper: 0, lower: 0 }
    }
  ],
  die: [],
  round: 1,
  turn: {
    player: PLAYER_ONE,
    status: TURN_STATUS.TURN_BEGIN
  },
  status: GAME_STATUS.NOT_IN_PROGRESS
};

function rootReducer(state = initialState, action) {
  const { type, payload } = action;
  const { players } = state;
  let playerIdx;

  switch (type) {
    case EDIT_PLAYER:
      playerIdx = players.findIndex(p => p.id === payload.id);
      players[playerIdx].name = payload.name;
      return Object.assign({}, state, { players });
    case ROLL_DIE: {
      const turn = { ...state.turn, status: TURN_STATUS.DICE_ROLLED };
      return Object.assign({}, state, { die: action.values, turn });
    }
    case SET_SCORE:
      const { playerId, rule, gameNum, value } = payload;
      playerIdx = players.findIndex(p => p.id === playerId);
      players[playerIdx].scores = players[playerIdx].scores.map(s => {
        if (s.rule === rule && s.gameNum === gameNum) {
          s.value = value;
        }
        return s;
      });
      players[playerIdx].total = {
        upper: sumSection(players, playerId, 'Upper'),
        lower: sumSection(players, playerId, 'Lower')
      };
      return Object.assign({}, state, { players });
    case SWITCH_TURN:
      const player = state.turn.player === PLAYER_ONE ? PLAYER_TWO : PLAYER_ONE;
      const turn = { player, status: TURN_STATUS.TURN_BEGIN };
      return Object.assign({}, state, { turn, die: [] });
    case START_GAME:
      return Object.assign({}, state, { status: GAME_STATUS.IN_PROGRESS });
    default:
      break;
  }

  return state;
}

export default rootReducer;
