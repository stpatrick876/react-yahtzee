import React from 'react';
import { connect } from 'react-redux';
import { PLAYER_ONE, PLAYER_TWO } from '../reducers/index';
import ScoreSheet from '../components/ScoreSheet/ScoreSheet';
import {
  playerSectionTotalSelector,
  playerCellScoreSelector
} from '../selectors/index';
import { editPlayer, setScore, switchTurn } from '../actions';

function mapDispatchToProps(dispatch) {
  return {
    editPlayer: player => dispatch(editPlayer(player)),
    setScore: score => dispatch(setScore(score)),
    switchTurn: () => dispatch(switchTurn())
  };
}

const mapStateToProps = state => {
  const { players, turn, status, die, round } = state;
  return {
    players,
    turn,
    status,
    die,
    round,
    getSectionTotal: playerSectionTotalSelector(state),
    getCellValue: playerCellScoreSelector(state)
  };
};

function ScoreSheets(props) {
  const {
    players,
    turn,
    status,
    getSectionTotal,
    die,
    round,
    getCellValue,
    setScore,
    switchTurn
  } = props;
  const [playerOne, playerTwo] = players;
  const cellProps = {
    die,
    round,
    getCellValue,
    setScore,
    switchTurn
  };
  return (
    <div className='ScoreSheets'>
      <ScoreSheet
        player={playerOne}
        isActiveTurn={turn.player === PLAYER_ONE}
        gameStatus={status}
        getSectionTotal={getSectionTotal}
        editPlayer={editPlayer}
        cellProps={cellProps}
      />
      <ScoreSheet
        player={playerTwo}
        isActiveTurn={turn.player === PLAYER_TWO}
        gameStatus={status}
        getSectionTotal={getSectionTotal}
        editPlayer={editPlayer}
        cellProps={cellProps}
      />
    </div>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(ScoreSheets);
