import { connect } from 'react-redux';
import GameHeader from '../components/GameHeader/GameHeader';
import { startGame, diceRoll } from '../actions';

const mapDispatchToProps = dispatch => ({
  diceRoll: values => dispatch(diceRoll(values)),
  startGame: () => dispatch(startGame())
});

const mapStateToProps = state => {
  const { status, turn } = state;
  return { status, turn };
};

const GameHeaderContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(GameHeader);

export default GameHeaderContainer;
