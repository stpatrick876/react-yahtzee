import sum from 'lodash/sum';

export const GAME_RULES_UPPER = [
  {
    id: "Upper-1",
    rule: {
      label: "Aces",
      icon: "fas fa-dice-one"
    },
    howToScore: "Count and Add Only Aces",
    checkRule(die) {
      let isMatched = false;
      let value = 0;
      const ones = die.filter(dice => dice === 1);
      isMatched = ones.length > 0;
      value = sum(ones);
      return {
        isMatched,
        value
      }
    }
  },
  { id: "Upper-2",
    rule: {
      label: "Twos",
      icon: "fas fa-dice-two"
    },
    howToScore: "Count and Add Only Twos",
    checkRule(die) {
      let isMatched = false;
      let value = 0;
      const twos = die.filter(dice => dice === 2);
      isMatched = twos.length > 0;
      value = sum(twos);
      return {
        isMatched,
        value
      }
    }
  },
  {
    id: "Upper-3",
    rule: {
      label: "Threes",
      icon: "fas fa-dice-three"
    },
    howToScore: "Count and Add Only Threes",
    checkRule(die) {
      let isMatched = false;
      let value = 0;
      const threes = die.filter(dice => dice === 3);
      isMatched = threes.length > 0;
      value = sum(threes);
      return {
        isMatched,
        value
      }
    }
  },
  {
    id: "Upper-4",
    rule: {
      label: "Fours",
      icon: "fas fa-dice-four"
    },
    howToScore: "Count and Add Only Fours",
    checkRule(die) {
      let isMatched = false;
      let value = 0;
      const fours = die.filter(dice => dice === 4);
      isMatched = fours.length > 0;
      value = sum(fours);
      return {
        isMatched,
        value
      }
    }
  },
  {
    id: "Upper-5",
    rule: {
      label: "Fives",
      icon: "fas fa-dice-five"
    },
    howToScore: "Count and Add Only Fives",
    checkRule(die) {
      let isMatched = false;
      let value = 0;
      const fives = die.filter(dice => dice === 5);
      isMatched = fives.length > 0;
      value = sum(fives);
      return {
        isMatched,
        value
      }
    }
  },
  {
    id: "Upper-6",
    rule: {
      label: "Sixes",
      icon: "fas fa-dice-six"
    },
    howToScore: "Count and Add Only Sixes",
    checkRule(die) {
      let isMatched = false;
      let value = 0;
      const fives = die.filter(dice => dice === 6);
      isMatched = fives.length > 0;
      value = sum(fives);
      return {
        isMatched,
        value
      }
    }
  }
];

export const GAME_RULES_LOWER = [
  {
    id: "Lower-1",
    rule: {
      label: "3 of a kind"
    },
    howToScore: "Add Total Of All Dice"
  },
  {
    id: "Lower-2",
    rule: {
      label: "4 of a kind"
    },
    howToScore: "Add Total Of All Dice"
  },
  {
    id: "Lower-3",
    rule: {
      label: "Full House"
    },
    howToScore: "SCORE 25"
  },
  {
    id: "Lower-4",
    rule: {
      label: "Sm. Straight (Sequence of 4)"
    },
    howToScore: "SCORE 30"
  },
  {
    id: "Lower-5",
    rule: {
      label: "Lg. Straight (Sequence of 5)"
    },
    howToScore: "SCORE 40"
  },
  {
    id: "Lower-6",
    rule: {
      label: "YAHTZEE (Sequence of 5)"
    },
    howToScore: "SCORE 50"
  }
];
