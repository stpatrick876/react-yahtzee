export const GAME_STATUS = {
  IN_PROGRESS: 'in-progress',
  NOT_IN_PROGRESS: 'not-in-progress'
};

export const TURN_STATUS = {
  TURN_BEGIN: 'turn-begin',
  DICE_ROLLED: 'dice-rolled'
};
