export const EDIT_PLAYER = 'EDIT_PLAYER';
export const ROLL_DIE = 'ROLL_DIE';
export const SET_SCORE = 'SET_SCORE';
export const SWITCH_TURN = 'SWITCH_TURN';
export const START_GAME = 'START_GAME';
