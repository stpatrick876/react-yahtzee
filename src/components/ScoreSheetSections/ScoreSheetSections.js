import React, { Component } from 'react';
import './ScoreSheetSections.scss';

export default class ScoreSheetSections extends Component {
  render() {
    const { children } = this.props;
    return <div className='ScoreSheetSections'>{children}</div>;
  }
}
