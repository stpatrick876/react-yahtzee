import React from 'react';
import ScoreCell from '../ScoreCell/ScoreCell';

export default class ScoreSheetSection extends React.Component {
  constructor(props) {
    super(props);
    const { label, playerId, getSectionTotal } = this.props;
    console.log('getSectionTotal ', getSectionTotal);
    const total = getSectionTotal(label, playerId);
    this.state = {
      total
    };
    this.onCellScored = this.onCellScored.bind(this);
  }

  onCellScored() {
    const { label, playerId, getSectionTotal } = this.props;
    const total = getSectionTotal(label, playerId);

    this.setState({ total });
  }

  render() {
    const {
      label,
      gameRules,
      playerId,
      isActiveTurn,
      cellProps: storeProps
    } = this.props;
    const { total } = this.state;
    const rows = gameRules.map((item, i) => {
      const {
        rule: { label, icon },
        howToScore,
        id
      } = item;
      return (
        <tr key={i}>
          <td>
            <span className='rule-label'>
              {label}
              <i className={icon}></i>
            </span>
          </td>
          <td>
            <p className='rule-desc'>{howToScore}</p>
          </td>
          <ScoreCell
            playerId={playerId}
            rule={id}
            gameNum={1}
            onCellScored={this.onCellScored}
            isActiveTurn={isActiveTurn}
            storeProps={storeProps}
          />
          <ScoreCell
            playerId={playerId}
            rule={id}
            gameNum={2}
            onCellScored={this.onCellScored}
            isActiveTurn={isActiveTurn}
            storeProps={storeProps}
          />
          <ScoreCell
            playerId={playerId}
            rule={id}
            gameNum={3}
            onCellScored={this.onCellScored}
            isActiveTurn={isActiveTurn}
            storeProps={storeProps}
          />
          <ScoreCell
            playerId={playerId}
            rule={id}
            gameNum={4}
            onCellScored={this.onCellScored}
            isActiveTurn={isActiveTurn}
            storeProps={storeProps}
          />
          <ScoreCell
            playerId={playerId}
            rule={id}
            gameNum={5}
            onCellScored={this.onCellScored}
            isActiveTurn={isActiveTurn}
            storeProps={storeProps}
          />
          <ScoreCell
            playerId={playerId}
            rule={id}
            gameNum={6}
            onCellScored={this.onCellScored}
            isActiveTurn={isActiveTurn}
            storeProps={storeProps}
          />
        </tr>
      );
    });

    return (
      <div className='ScoreSheetSection'>
        <label className='ScoreSheetSection__title'>
          {label}(Total: {total})
        </label>
        <table className='rules-table'>
          <thead>
            <tr>
              <th>Rule</th>
              <th>How to Score</th>
              <th>
                Game <br /> #1
              </th>
              <th>
                Game <br /> #2
              </th>
              <th>
                Game <br /> #3
              </th>
              <th>
                Game <br /> #4
              </th>
              <th>
                Game <br /> #5
              </th>
              <th>
                Game <br /> #6
              </th>
            </tr>
          </thead>
          <tbody>{rows}</tbody>
        </table>
      </div>
    );
  }
}
