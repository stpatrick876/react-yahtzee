import React, { Component } from 'react';
import DieBoard from '../DieBoard/DieBoard';
import './GameHeader.scss';
import { GAME_STATUS } from '../../constants/game';

export default class GameHeader extends Component {
  render() {
    const { status, startGame, turn, diceRoll } = this.props;
    return (
      <header className='App-header'>
        <h1>Yahtzee</h1>
        {status === GAME_STATUS.IN_PROGRESS ? (
          <DieBoard
            className='App-content__die-board'
            turn={turn}
            diceRoll={diceRoll}
          />
        ) : (
          <button className='button' onClick={startGame}>
            Start Game
          </button>
        )}
      </header>
    );
  }
}
