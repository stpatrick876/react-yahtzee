/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import React from 'react';
import renderer from 'react-test-renderer';
import { render } from '@testing-library/react';
import GameHeader from './GameHeader';
import { GAME_STATUS } from '../../constants/game';

describe.only('GameHeader test', () => {
  it('GameHeader should match snapshot', () => {
    const component = renderer.create(<GameHeader />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should show start Game button', () => {
    const { getByText } = render(<GameHeader />);
    expect(getByText('Start Game')).toBeInTheDocument();
  });

  it('should show die when game started', () => {
    const { getByText } = render(
      <GameHeader status={GAME_STATUS.IN_PROGRESS} turn={{ status: '' }} />
    );
    expect(getByText('Roll Dice')).toBeInTheDocument();
  });
});
