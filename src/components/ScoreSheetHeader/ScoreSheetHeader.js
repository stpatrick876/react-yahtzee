import React, { Component, Fragment } from 'react';
import './ScoreSheetHeader.scss';

export default class ScoreSheetHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.player.name,
      isEdit: false
    };
    this.onNameChange = this.onNameChange.bind(this);
  }

  onNameChange(e) {
    const {
      target: { value: name }
    } = e;
    const { editPlayer, player } = this.props;
    this.setState({ name });
    editPlayer({ ...player, name });
  }

  render() {
    const { name, isEdit } = this.state;
    const {
      player: { total }
    } = this.props;
    return (
      <div className='ScoreSheetHeader'>
        <label htmlFor='name'>Name: </label>
        {isEdit ? (
          <input
            id='name'
            value={name}
            onChange={this.onNameChange}
            onBlur={() => this.setState({ isEdit: false })}
          />
        ) : (
          <Fragment>
            <span className='player-name'>{name}</span>
            <i
              className='fa fa-edit'
              onClick={() => this.setState({ isEdit: true })}
            ></i>
          </Fragment>
        )}
        <div className='player-total'>
          <label>Total: </label>
          <span className='player-total'>{total.upper + total.lower}</span>
        </div>
      </div>
    );
  }
}
