import React, { Component } from 'react';
import ScoreSheetHeader from '../ScoreSheetHeader/ScoreSheetHeader';
import './ScoreSheet.scss';
import ScoreSheetSections from '../ScoreSheetSections/ScoreSheetSections';
import ScoreSheetSection from '../ScoreSheetSection/ScoreSheetSection';
import { GAME_RULES_UPPER, GAME_RULES_LOWER } from '../../constants/game-rules';
import { GAME_STATUS } from '../../constants/game';

export default class ScoreSheet extends Component {
  render() {
    const {
      player,
      isActiveTurn,
      gameStatus,
      getSectionTotal,
      editPlayer,
      cellProps
    } = this.props;
    return (
      <div
        className={`ScoreSheet ${isActiveTurn &&
          'ScoreSheet--active-player'} ScoreSheet--${gameStatus}`}
      >
        <ScoreSheetHeader player={player} editPlayer={editPlayer} />
        {gameStatus === GAME_STATUS.IN_PROGRESS && (
          <ScoreSheetSections>
            <ScoreSheetSection
              playerId={player.id}
              label={'Upper'}
              gameRules={GAME_RULES_UPPER}
              isActiveTurn={isActiveTurn}
              getSectionTotal={getSectionTotal}
              cellProps={cellProps}
            />
            <ScoreSheetSection
              playerId={player.id}
              label={'Lower'}
              gameRules={GAME_RULES_LOWER}
              isActiveTurn={isActiveTurn}
              getSectionTotal={getSectionTotal}
              cellProps={cellProps}
            />
          </ScoreSheetSections>
        )}
      </div>
    );
  }
}
