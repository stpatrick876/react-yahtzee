import React, { Component } from 'react';
import './DieBoard.scss';
import ReactDice from 'react-dice-complete';
import 'react-dice-complete/dist/react-dice-complete.css';

export default class DieBoard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRolling: false
    };
    this.onRollDie = this.onRollDie.bind(this);
    this.onRollComplete = this.onRollComplete.bind(this);
  }
  onRollDie() {
    this.setState({ isRolling: true }, () => this.reactDice.rollAll());
  }
  onRollComplete(total, values) {
    this.setState({ isRolling: false });
    this.props.diceRoll(values);
  }
  render() {
    const { isRolling } = this.state;
    const { turn } = this.props;
    return (
      <div
        className={`DieBoard DieBoard--${turn.status} ${isRolling &&
          'DieBoard--rolling'}`}
      >
        <button
          className='button'
          onClick={this.onRollDie}
          disabled={isRolling}
        >
          Roll Dice
          <i className='fas fa-dice'></i>
        </button>

        <ReactDice
          numDice={5}
          faceColor={'rgb(12, 3, 54)'}
          dotColor='#ffffff'
          disableIndividual
          rollDone={this.onRollComplete}
          ref={dice => (this.reactDice = dice)}
        />
      </div>
    );
  }
}
