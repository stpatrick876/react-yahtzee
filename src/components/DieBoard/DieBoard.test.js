/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import React from 'react';
import renderer from 'react-test-renderer';
import { render } from '@testing-library/react';
import DieBoard from './DieBoard';
import { GAME_STATUS } from '../../constants/game';

describe.only('DieBoard test', () => {
  it('DieBoard should match snapshot', () => {
    const component = renderer.create(<DieBoard turn={{ status: '' }} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
  it('should roll die', () => {
    const { getByText } = render(<DieBoard turn={{ status: '' }} />);

    const btn = getByText('Roll Dice');
    btn.click();
  });
});
