import React from 'react';
import './App.scss';
import GameHeaderContainer from '../../containers/GameHeaderContainer';
import ScoreSheets from '../../containers/ScoreSheetsContainer';

export default function App() {
  return (
    <div className='App'>
      <GameHeaderContainer />
      <div className='App-content'>
        <ScoreSheets />
      </div>
    </div>
  );
}
