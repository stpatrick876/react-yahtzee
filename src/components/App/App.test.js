// Auto-generated do not edit

/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-undef */
import React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import App from './App';
import { initScores } from '../../utils';
import { GAME_STATUS, TURN_STATUS } from '../../constants/game';
const mockStore = configureMockStore();
export const PLAYER_ONE = 'one';
export const PLAYER_TWO = 'two';
const store = mockStore({
  players: [
    {
      id: PLAYER_ONE,
      name: 'Player 1',
      scores: initScores(),
      total: { upper: 0, lower: 0 }
    },
    {
      id: PLAYER_TWO,
      name: 'Player 2',
      scores: initScores(),
      total: { upper: 0, lower: 0 }
    }
  ],
  die: [],
  round: 1,
  turn: {
    player: PLAYER_ONE,
    status: TURN_STATUS.TURN_BEGIN
  },
  status: GAME_STATUS.NOT_IN_PROGRESS
});

describe.only('App test', () => {
  it('App should match snapshot', () => {
    const component = renderer.create(
      <Provider store={store}>
        <App />
      </Provider>
    );
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
