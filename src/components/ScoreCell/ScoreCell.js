import React, { Component } from 'react';
import { GAME_RULES_UPPER, GAME_RULES_LOWER } from '../../constants/game-rules';
import './ScoreCell.scss';

export default class ScoreCell extends Component {
  constructor(props) {
    super(props);
    const {
      storeProps: { getCellValue },
      rule,
      gameNum,
      playerId
    } = props;

    const content = getCellValue(playerId, rule, gameNum);
    this.state = {
      activeHover: false,
      cellHoverValue: '',
      content,
      cellNoMatchOnHover: false,
      rule: [...GAME_RULES_UPPER, ...GAME_RULES_LOWER].filter(
        r => r.id === props.rule
      )[0]
    };

    this.onMouseEnterCell = this.onMouseEnterCell.bind(this);
    this.onMouseLeaveCell = this.onMouseLeaveCell.bind(this);
    this.onCellClick = this.onCellClick.bind(this);
  }

  onMouseEnterCell(e) {
    const {
      gameNum,
      storeProps: { die, round }
    } = this.props;
    const { rule, content } = this.state;

    if (die.length < 1 || round !== gameNum || content) {
      return false;
    }
    this.setState({ activeHover: true });
    if (rule && rule.checkRule) {
      const checkRule = rule.checkRule(die);
      if (checkRule.isMatched) {
        this.setState({ cellHoverValue: checkRule.value });
      } else {
        this.setState({ cellNoMatchOnHover: true });
      }
    }
  }

  onMouseLeaveCell(e) {
    this.setState({
      activeHover: false,
      cellHoverValue: '',
      cellNoMatchOnHover: false
    });
  }

  onCellClick() {
    const {
      rule,
      gameNum,
      playerId,
      storeProps: { switchTurn, setScore },
      onCellScored
    } = this.props;
    const { cellHoverValue, content } = this.state;

    if (!cellHoverValue || content) return false;

    this.setState({ content: cellHoverValue }, () => {
      const { content: value } = this.state;
      setScore({
        playerId,
        rule,
        gameNum,
        value
      });
    });

    setTimeout(() => {
      switchTurn();
      onCellScored();
    }, 1000);
  }

  render() {
    const {
      activeHover,
      cellHoverValue,
      cellNoMatchOnHover,
      content
    } = this.state;
    const { isActiveTurn } = this.props;
    return (
      <td
        className={`Score-Cell ${activeHover &&
          'active-hover'} ${cellNoMatchOnHover && 'no-match'} ${isActiveTurn ||
          'not-is-active-turn'}`}
        onMouseEnter={this.onMouseEnterCell}
        onMouseLeave={this.onMouseLeaveCell}
        onClick={this.onCellClick}
      >
        {!isActiveTurn ? content : content ? content : cellHoverValue}
      </td>
    );
  }
}
