import { GAME_RULES_UPPER, GAME_RULES_LOWER } from "./constants/game-rules";
import sum from 'lodash/sum';

export const initScores = () => {
  const scores = [];
  [...GAME_RULES_UPPER, ...GAME_RULES_LOWER].forEach(r => {
    for (let i = 0; i < 6; i++) {
      scores.push({
        rule: r.id,
        gameNum: i + 1
      });
    }
  });
  return scores;
}

export const sumSection = (players, playerId, section) => {
    const player = players.filter(p => p.id === playerId)[0];
    const {scores} = player;
    const values = scores.filter(s => s.rule.split('-')[0] === section).map(s => s.value || 0);
    return sum(values);
};
