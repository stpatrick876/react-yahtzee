import { createSelector } from 'reselect';

const playersSelector = state => state.players;

export const playerCellScoreSelector = createSelector(
  playersSelector,
  players => (playerId, rule, gameNum) => {
    const player = players.filter(p => p.id === playerId)[0];
    const { scores } = player;
    return scores.filter(s => s.rule === rule && s.gameNum === gameNum)[0]
      .value;
  }
);

export const playerSectionTotalSelector = createSelector(
  playersSelector,
  players => (section, playerId) => {
    const player = players.filter(p => p.id === playerId)[0];
    return player.total[section.toLowerCase()];
  }
);
