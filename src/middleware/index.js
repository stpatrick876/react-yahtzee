import { SET_SCORE } from "../constants/action-types";
import { switchTurn } from "../actions/index";

export function gameTurnMiddleware({ dispatch }) {
  return function(next) {
    return function(action) {
      const { type } = action;
      switch (type) {
        case SET_SCORE:
          return dispatch(switchTurn());

        default:
          break;
      }

      return next(action);
    };
  };
}
